# SFML Simple Tetris Game ReadMe

# Editing this README
Open source project (C++ / SFML framework). 

## Description
Project can help me to study C++, GameDev, SFML framework.

## Visuals
You can see screenshots and short video.

## Installation
Best way of installing SFML and test project code I can find here:
www.sfml-dev.org
https://github.com/SFML/cmake-sfml-project

## Support
You can go to linkes below for help (Discord SFML chanel):
https://discord.gg/aAneaXnYVY

## Project status
This project will keep going :)
